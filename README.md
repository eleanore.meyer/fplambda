Small implementation of the evaluation of lambda terms as discussed in the functional programming lecture.
You can write applications as:

```
λx. y
```

or in a more haskell like syntax:

```
\x -> y
```

The implementation either assumes the first argument to be a path to the input file
or the input beeing given by stdin (e.g. `echo "(λx. x) zero" | ./fplambda`).
