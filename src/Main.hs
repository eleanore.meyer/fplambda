{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE LambdaCase        #-}

module Main where

import System.Environment
import Data.Text (Text)
import qualified Data.Text as T
import Data.Void
import Text.Megaparsec
import Text.Megaparsec.Char
import Data.Set (Set)
import qualified Data.Set as S

data Lambda = Var Text
            | App Lambda Lambda
            | Abs Text   Lambda
            deriving Eq

instance Show Lambda where
  show (Var n   ) = T.unpack n
  show (App l l') = "(" ++ show l ++ " " ++ show l' ++ ")"
  show (Abs n l ) = "(\\" ++ T.unpack n ++ ". " ++ show l ++ ")"

-- parsing
type Parser = Parsec Void Text

parens, spaced :: Parser a -> Parser a
parens = between (char '(') (char ')')
spaced p = (space *> p) <* space

pName :: Parser Text
pName = T.pack <$> some alphaNumChar

pVar :: Parser Lambda
pVar = Var <$> pName

pApp :: Parser Lambda
pApp =
  some (spaced (pVar <|> parens pTerm)) >>= \case
    l@(_:_:_) -> pure $ foldl1 App l
    _         -> empty

pAbs :: Parser Lambda
pAbs = Abs <$>
  (spaced startSeq *> spaced pName) <*> (spaced conn *> spaced pTerm)
  where
    conn = string "->" <|> string "."
    startSeq = char '\\' <|> char 'λ'

pTerm :: Parser Lambda
pTerm = try pAbs <|> try pApp <|> try pVar <|> try (parens pTerm)

parserTest :: String -> IO ()
parserTest = parseTest pTerm . T.pack

-- evaluation
free :: Lambda -> Set Text
free (Var n   ) = S.singleton n
free (App l l') = S.union (free l) (free l')
free (Abs n l ) = S.delete n (free l)

substitute :: Lambda -> Text -> Lambda -> Lambda
substitute r@(Var n) x t
  | x==n       = t
  | otherwise  = r
substitute (App l l') x t = App (substitute l x t) (substitute l' x t)
substitute r@(Abs n l ) x t
  | x==n                    = r
  | S.notMember n (free t)  = Abs n (substitute l x t)
  | otherwise               =
      -- we construct a new unique name
      let newName = S.findMax (S.union (free l) (free t)) `T.append` "'" in
      Abs newName (substitute (substitute l n $ Var newName) x t)

normalize :: Lambda -> Lambda
normalize (App l l') = case normalize l of
  Abs n r -> normalize (substitute r n l')
  left    -> App left (normalize l')
normalize (Abs n r)  = Abs n (normalize r)
normalize var        = var

parseGet :: Text -> Lambda
parseGet inp = case runParser pTerm "" inp of
  Left e -> error (errorBundlePretty e)
  Right l -> l

main :: IO ()
main =
  getArgs >>= \case
    []    -> (,) "(stdin)" <$> getContents
    (x:_) -> (,) x <$> readFile x
  >>= \(filename,inp) -> case runParser pTerm filename (T.pack inp) of
    Left  e -> putStrLn (errorBundlePretty e)
    Right l -> print l *> putStrLn ("normalized: " ++ show (normalize l))
